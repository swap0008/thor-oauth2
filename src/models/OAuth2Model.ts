import { FirestoreClient } from './FirestoreClient.js';

export class OAuth2Model extends FirestoreClient {
    static GOOGLE_ACCOUNTS = 'google_accounts';
    static ACCOUNT_ID = 'account_id';

    constructor() {
        super('oauth2');
    }
}
