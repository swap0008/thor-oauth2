import { applyMiddleware, verifyJwtToken } from 'middlewares-nodejs';
import { startOAuth, finishOAuth, getServices } from '../controller/controller.js';
import { startFacebookOAuthController, finishFacebookOAuthController } from '../controller/facebook.js';

export const healthCheck = (event, context, callback) => {
    const response = {
        statusCode: 200,
        body: JSON.stringify(`You're being served from ap-south-1 Mumbai region. (OAuth2API)`)
    }

    callback(null, response);
}

export const start = (event, context, callback) => {
    applyMiddleware(event, callback, [], startOAuth);
}

export const finish = (event, context, callback) => {
    applyMiddleware(event, callback, [], finishOAuth);
}

export const startFacebookOAuth = (event, context, callback) => {
    applyMiddleware(event, callback, [], startFacebookOAuthController);
}

export const finishFacebookOAuth = (event, context, callback) => {
    applyMiddleware(event, callback, [], finishFacebookOAuthController);
}

export const services = (event, context, callback) => {
    applyMiddleware(event, callback, [], getServices);
}