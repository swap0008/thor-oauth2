import * as GoogleApis from 'googleapis';
import { credentials } from '../Credentials.js';
import { FirestoreClient } from '../models/FirestoreClient.js';
import { OAuth2Model } from '../models/OAuth2Model.js';
import { PEOPLE_SERVICE, CALENDAR_SERVICE } from './constants.js';

const Company = new FirestoreClient('companies');
const Employee = new FirestoreClient('employees');
const OAuth2 = new OAuth2Model();

const { google } = GoogleApis;
const { client_secret, client_id, redirect_uris } = credentials.web;
const oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[4]);


const defaultScopes = [
    'https://www.googleapis.com/auth/userinfo.email'
];

export const response = (callback, statusCode, body) => {
    return callback(null, {
        statusCode: statusCode || 200,
        body: JSON.stringify(body, (key, val) => {
            if (typeof val === 'function') return `(${val})`;
            return val;
        })
    });
}

export const startOAuth = async (event, callback) => {
    const { account_id } = event.pathParameters || {};
    let { scopes = '' } = event.queryStringParameters || {};

    scopes = scopes.split(',');

    let state = {};
    state = { account_id };

    console.log(account_id);

    const [company, employee] = await Promise.all([Company.getDoc(account_id), Employee.getDoc(account_id)]);
    console.log(company.exists);
    console.log(employee.exists);
    if (!(company.exists || employee.exists)) return response(callback, 400, 'Company or Employee does not exists!');

    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: defaultScopes.concat(scopes.map(scope => `https://www.googleapis.com/auth/${scope}`)),
        state: JSON.stringify({ ...state, scopes })
    });

    console.log(authUrl);
    response(callback, 200, authUrl);
}

export const finishOAuth = async (event, callback) => {
    const { code, state } = event.queryStringParameters || {};

    const { tokens } = await oAuth2Client.getToken(code);
    const { account_id, scopes } = JSON.parse(state);

    oAuth2Client.setCredentials(tokens);

    const service = google.oauth2({ version: 'v1', auth: oAuth2Client });

    const { data: { email } } = await service.userinfo.get();

    let query = OAuth2.collectionRef.where(OAuth2Model.ACCOUNT_ID, '==', account_id);
    const snapshot = await query.get();

    const secrets = {};
    scopes.forEach(scope => {
        secrets[scope] = { access_token: tokens.access_token, refresh_token: tokens.refresh_token }
    });

    if (snapshot.empty) {
        let body = {
            [OAuth2Model.ACCOUNT_ID]: account_id,
            [OAuth2Model.GOOGLE_ACCOUNTS]: {
                [`${email}`]: secrets
            }
        };

        const doc = await OAuth2.add(body);
        return response(callback, 200, doc.id);
    }

    let { google_accounts, id } = snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }))[0];
    google_accounts = {
        ...google_accounts,
        [`${email}`]: {
            ...google_accounts[`${email}`],
            ...secrets
        }
    };

    await OAuth2.update(id, { google_accounts });
    response(callback, 200, 'Google account added!');
}

export const getServices = async (event, callback) => {
    const { account_id, accounts, type }: any = event.body;

    let snapshot = await OAuth2.collectionRef.where(OAuth2Model.ACCOUNT_ID, '==', account_id).get();
    if (snapshot.empty) return response(callback, 400, 'No matching document.');

    const { google_accounts } = snapshot.docs.map((doc: any) => doc.data())[0];

    const tokens: string[] = [];
    accounts.forEach((account: string) => {
        const gmail = google_accounts[account]; 

        if (!gmail) return;

        let refresh_token: string;
        switch (type) {
            case PEOPLE_SERVICE:
                refresh_token = gmail['contacts']['refresh_token'];
                tokens.push(refresh_token);
                break;
            case CALENDAR_SERVICE:
                refresh_token = gmail['calendar']['refresh_token'];
                tokens.push(refresh_token);
                break;
            default:
                break;
        }
    });

    response(callback, 200, tokens);
}