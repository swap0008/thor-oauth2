import { create } from 'simple-oauth2';
import { response } from './controller.js';
import Axios from 'axios';
import * as querystring from 'query-string';
import { OAuth2Model } from '../models/OAuth2Model.js';

const facebook = {
    appId: '181254539834256',
    appSecret: '889f48bb6128e2d8dc6cea051e494d6d',
    redirectUrl: 'http://localhost:4000/facebook/finish'
};

const OAuth2 = new OAuth2Model();

function oauth2Client() {
    // Instagram OAuth 2 setup
    // TODO: Configure the `instagram.client_id` and `instagram.client_secret` Google Cloud environment constiables.
    const credentials = {
        client: {
            id: facebook.appId, // functions.config().instagram.client_id,
            secret: facebook.appSecret // functions.config().instagram.client_secret
        },
        auth: {
            authorizeHost: 'https://www.facebook.com',
            authorizePath: '/dialog/oauth',
            tokenHost: 'https://graph.facebook.com',
            tokenPath: '/oauth/access_token'
        },
        http: {
            'headers.Accept': 'application/json'
        }
    };
    return create(credentials);
}

const OAUTH_SCOPES = 'manage_pages, publish_pages, leads_retrieval, ads_read, pages_messaging, ads_management';

export const startFacebookOAuthController = (event, callback) => {
    const { business_id } = event.queryStringParameters || {};

    if (!business_id) return response(callback, 400, 'Business id required.');

    const oauth2 = oauth2Client();
    const redirectUri = oauth2.authorizationCode.authorizeURL({
        redirect_uri: facebook.redirectUrl,
        scope: OAUTH_SCOPES,
        state: JSON.stringify({ business_id })
    });

    console.log(redirectUri);

    callback(null, {
        statusCode: 301,
        headers: {
            Location: redirectUri
        }
    });
}

export const finishFacebookOAuthController = async (event, callback) => {
    let { code, state } = event.queryStringParameters || {};

    state = JSON.parse(state);

    const form = {
        grant_type: 'authorization_code',
        code,
        redirect_uri: facebook.redirectUrl
    };

    const formData = querystring.stringify(form);
    const contentLength = formData.length;

    const { data } = await Axios({
        method: 'post',
        url: 'https://graph.facebook.com/oauth/access_token',
        headers: {
            'Content-Length': contentLength,
            'Content-Type': 'application/x-www-form-urlencoded',
            Authorization: 'Basic ' + new Buffer(facebook.appId + ':' + facebook.appSecret).toString('base64')
        },
        data: formData
    });

    const saveData = {
        access_token: data.access_token,
        business_id: state.business_id
    }

    const doc = await OAuth2.add(saveData);

    saveData['id'] = doc.id;

    response(callback, 200, saveData);
}